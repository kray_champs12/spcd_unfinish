/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SPCD;

import java.awt.GridLayout;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

public class Loading extends JPanel {

  JProgressBar pbar;

  static final int MY_MINIMUM = 0;

  static final int MY_MAXIMUM = 100;

  public Loading() {
    pbar = new JProgressBar();
    pbar.setMinimum(MY_MINIMUM);
    pbar.setMaximum(MY_MAXIMUM);
    add(pbar);
    pbar.setStringPainted(true);
    
  }

  public void updateBar(int newValue) {
    pbar.setValue(newValue);
  }
  public static void main(String args[]) {

    final Loading it = new Loading();

    JFrame frame = new JFrame("Loading....");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setLocationRelativeTo(null);
    frame.setResizable(false);
    frame.setContentPane(it);
    frame.pack();
    frame.setVisible(true);
    frame.setLayout(new GridLayout(1,1,8,8));

    for (int i = MY_MINIMUM; i <= MY_MAXIMUM; i++) {
      final int percent = i;
      try {
        SwingUtilities.invokeLater(new Runnable() {
          public void run() {
            it.updateBar(percent);
          }
        });
        java.lang.Thread.sleep(100);
      } catch (InterruptedException e) {
        ;
      }
      if(percent==100){
            menu x = new menu();
            x.setVisible(true);
            x.setResizable(false);
            x.setLocation(0,0);
            x.setExtendedState( x.MAXIMIZED_BOTH );
            x.setTitle("Log in First!");
            frame.dispose();
      }    
    }
    
  }
}
